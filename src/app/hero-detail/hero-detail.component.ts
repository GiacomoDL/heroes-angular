import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroService } from '../hero.service';
import { AbilityService } from '../ability.service';

import { Hero } from '../hero';
import { Ability } from '../ability';
import { FormControl, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {

  email = new FormControl('', [Validators.email]);
  potere = new FormControl('', [Validators.required]);
  descrizione = new FormControl('', [Validators.required]);


  hero: Hero;
  abilityNotOwned: Ability[];
  listAbility: Ability[];
  panelOpenState: boolean = false;
  duplicateAbility: boolean;

  constructor(private route: ActivatedRoute,
    private heroService: HeroService, private abilityService: AbilityService, private router: Router) { }

  ngOnInit() {
    this.duplicateAbility = false;
    if (+this.route.snapshot.paramMap.get('id') > 0)
      this.getHero();
    else
      this.getHeroByName();
  }

  sortByName() {
    var element = <HTMLInputElement>document.getElementById("sortButton")

    if (element.getAttribute("value") == "a-z") {
      this.hero.listAbility.sort((a: Ability, b: Ability) => a.name > b.name ? 1 : -1)
      element.setAttribute("value", "z-a");
    }
    else {
      this.hero.listAbility.sort((a: Ability, b: Ability) => b.name > a.name ? 1 : -1)
      element.setAttribute("value", "a-z");
    }
    this.getListAbility();
  }

  getErrorMessageEmail() {
    return this.email.hasError('email') ? 'Campo email non valido' :
      '';
  }

  getErrorMessagePotere() {
    return this.potere.hasError('required') ? 'Campo potere richiesto' :
      '';
  }

  getErrorMessageDescription() {
    return this.descrizione.hasError('required') ? 'Campo descrizione richiesto' :
      '';
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
      .subscribe(data => {
        this.hero = data;
        this.getListAbility();
      },
        error => {
          if (error instanceof HttpErrorResponse && error.status == 404) {
            console.log("errore")
            this.router.navigateByUrl('/not-found', {replaceUrl: true});
          }
        })
  }

  getHeroByName() {
    var name = this.route.snapshot.paramMap.get('name');
    this.heroService.findByName(name)
      .subscribe(data => {
        this.hero = data;
        this.getListAbility();
      },
      error => {
        if (error instanceof HttpErrorResponse && error.status == 404) {
          console.log("errore")
          this.router.navigateByUrl('/not-found', {replaceUrl: true});
        }
      })
  }

  updateEmail(hero: Hero) {
    this.heroService.updateEmail(hero.id, hero.email).subscribe();
  }

  deleteAbility(idHero: number, idAbility: number) {
    this.abilityService.deleteAbility(idHero, idAbility).subscribe(data => this.ngOnInit());
  }

  getListAbility() {
    this.abilityService.getAll().subscribe(data => this.abilityNotOwned =
      data.filter(item => !this.hero.listAbility.some(other => item.name === other.name)));
  }

  addAbilityToHero(idHero: number, idAbility: number) {
    this.abilityService.addAbilityToHero(idHero, idAbility).subscribe(data => this.ngOnInit())
  }

  addAbilityToDb(abilityName: string, abilityDescription: string, idHero: number) {
    this.duplicateAbility = false;
    var ability = { "id": 0, "name": abilityName, "description": abilityDescription, "listHero": null };
    this.abilityService.addAbilityToDb(ability, idHero).subscribe(data => {this.ngOnInit()},
    error => {
      if (error instanceof HttpErrorResponse && error.status == 403) {
        this.duplicateAbility = true;
      }
    })
}

  deleteAbilityFromDb(idAbility: number, nameAbility: string) {
    if (confirm("Eliminare definitivamente l'abilità " + nameAbility)) {
      this.abilityService.deleteAbilityFromDb(idAbility).subscribe(data => this.ngOnInit())
    }
  }
}
