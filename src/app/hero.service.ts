import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Hero } from './hero';
import { HttpClient, HttpHeaders } from '@angular/common/http'


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private url = 'https://giacomo-heroes.herokuapp.com/hero';
  //private url = 'http://localhost:8080/hero';


  constructor(private http: HttpClient) {
  }

  getHeroes(): Observable<Hero[]> {
    // TODO: send the message _after_ fetching the heroes
    return this.http.get<Hero[]>(this.url + "/all");
  }

  addHero(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(this.url + "/add", hero, httpOptions);
  }

  updateEmail(id: number, email: string): Observable<Hero> {
    return this.http.post<Hero>(this.url + "/updateEmail", { id, email }, httpOptions);
  }

  delete(id: number) {
    return this.http.delete(this.url + "/delete/" + id);
  }

  getHero(id: number) {
    return this.http.get<Hero>(this.url + "/find/" + id);
  }

  findByName(name: string){
    return this.http.get<Hero>(this.url + "/search/" + name);
  }

}
