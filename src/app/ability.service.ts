import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Ability } from './ability';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AbilityService {

  private url = 'https://giacomo-heroes.herokuapp.com/hero';
  //private url = 'http://localhost:8080/hero';


  constructor(private http: HttpClient) {
  }

  getAll(): Observable<Ability[]> {
    return this.http.get<Ability[]>(this.url + "/allAbility");
  }

  deleteAbility(idHero: number, idAbility: number): Observable<Ability> {
    return this.http.delete<Ability>(this.url + "/deleteAbility/" + idHero + "/" + idAbility)
  }
  addAbilityToHero(idHero: number, idAbility: number): Observable<Ability> {
    return this.http.post<Ability>(this.url + "/addAbilityToHero/" + idHero + "/" + idAbility, httpOptions)
  }
  addAbilityToDb(ability: Ability, idHero: number): Observable<Ability> {
    return this.http.post<Ability>(this.url + "/addAbilityToDb/" + idHero, ability, httpOptions)
  }
  deleteAbilityFromDb(idAbility: number) {
    return this.http.delete<Ability>(this.url + "/deleteAbilityFromDb/" + idAbility);
  }

}
