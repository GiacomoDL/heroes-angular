import { Ability } from "./ability";

export class Hero {
  // nome proprieta': tipo;
  id: number;
  name: string;
  email: string;
  listAbility: Ability[];
}
