import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})

export class HeroesComponent implements OnInit {
  emailHero = new FormControl('', [Validators.email]);
  nameHero = new FormControl('', [Validators.required, Validators.minLength(3)]);

  selectedHero: Hero;
  heroes: Hero[];
  newHero: Hero = new Hero;
  listNames: string[];
  myControl = new FormControl();
  filteredOptions: Observable<any[]>;
  duplicateName: boolean;

  constructor(private heroService: HeroService, private router: Router) {
  }

  ngOnInit() {
    this.duplicateName = false;
    this.getHeroes();
  }

  getErrorMessageName() {
    return this.nameHero.hasError('required') ? 'Campo nome richiesto' :
      this.nameHero.hasError('minlength') ? 'Minimo tre caratteri' :
        '';
  }

  getErrorMessageEmail() {
    return this.emailHero.hasError('email') ? 'Campo email non valido' :
      '';
  }

  filter() {
    this.myControl = new FormControl();
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(""),
        map(term => this._filter(term))
      );
  }

  private _filter(value: string) {
    if (this.heroes) {
      const filterValue = value.toLowerCase();
      return this.heroes.filter(hero => hero.name.toLowerCase().includes(filterValue));
    }
  }
  getHeroes() {
    this.heroService.getHeroes()
      .subscribe((heroes) => this.heroes = (heroes.sort((a: Hero, b: Hero) => a.id > b.id ? 1 : -1)))
  }

  sortAsc() {
    this.heroes.sort((a, b) =>
      a.name.toLowerCase().localeCompare(b.name.toLowerCase())
    )
  }

  add(name: string, email: string): void {
    this.duplicateName = false;
    var hero = { "id": 0, "name": name, "email": email, "listAbility": null };
    this.heroService.addHero(hero)
      .subscribe(data => {
        this.getHeroes()
      },
        error => {
          if (error instanceof HttpErrorResponse && error.status == 403) {
            this.duplicateName = true;
          }
        })
  }

  delete(id: number, name: string): void {
    if (confirm("Sicuro di voler eliminare l'eroe " + name + "?")) {
      this.heroService.delete(id).subscribe(data => this.getHeroes());
    }
  }

  onSelect(hero: Hero) {
    this.selectedHero = hero;
  }
}

