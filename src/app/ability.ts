import { Hero } from "./hero";

export class Ability {
  // nome proprieta': tipo;
  id: number;
  name: string;
  description: string;
  listHero: Hero[]; 
}
